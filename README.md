webyll
======

Webpack plugin to convert Markdown files to JSON. Loosely inspired by Jekyll.

Installing
==========

```
npm install https://gitlab.com/wbushey/webyll.git --save-dev
```

Use
===

```
const Webyll = require('webyll');

module.exports = {
  plugins: [
    new Webyll({
      /* Define the folder that contains your markdown files */
      inRootPath: path.join(__dirname, 'content'),
      /* Define the folder that JSON should be saved in */
      outRootPath: path.join(__dirname, 'public'),
    }),
  ],
}

```

Development
===========

## Testing

```
npm run test
```

## Lint

```
npm run lint
```

## Type Checking

```
npm run flow
```
